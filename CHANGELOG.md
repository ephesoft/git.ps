# CHANGELOG

## Deprecated features
*These will be removed in the next major release*
 - None

## 1.0.0 (2021-03-22)
 - Added initial release

- - - - -
Check the [Markdown Syntax Guide](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html) for basic syntax.