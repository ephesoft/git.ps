Describe 'ScriptAnalyzer' {
  Context 'Validating ScriptAnalyzer installation' {
    It 'Checking Invoke-ScriptAnalyzer exists.' {
      { Get-Command Invoke-ScriptAnalyzer -ErrorAction Stop } | Should -Not -Throw
    }
  }
}

Describe 'ScriptAnalyzer issues found' {

  $ExcludedFiles = @('tests')
  $ScriptAnalyzerSettings = @{

    <#
      Add justification for exclusions here
    #>
    ExcludeRules = @()
  }
  $results = Get-ChildItem .\* -Exclude $ExcludedFiles  | Invoke-ScriptAnalyzer -Settings $ScriptAnalyzerSettings
  $scripts = $results.ScriptName | Get-Unique

  Context 'Checking results' {
    It 'Should have no issues' {
      $results.count | Should -Be 0
    }
  }

  foreach ($script in $scripts) {
    Context $script {
      $issues = $results | Where-Object { $_.ScriptName -eq $script }

      foreach ($issue in $issues) {
        It "On line: $($issue.Line) - $($issue.Message)" {
          $true | Should -Be $False
        }
      }
    }
  }
}