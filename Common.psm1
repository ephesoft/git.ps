function Invoke-NativeCommand{
    [CmdletBinding()]
    Param (
        [Parameter (Mandatory = $true)]
        [string]$Command,
        [string[]]$Arguments
    )
    # Check for command
    try {
        Get-Command $Command | Out-Null
    }
    catch {
        Throw "Command $Command not found.  Please make sure the tool is installed and accessible."
    }

    Write-Verbose "Executing: $Command $Arguments"
    if($Env:NATIVE_COMMAND_CONFIRM -eq $true){
        Read-Host "`nPress any key to run command: $Command $Arguments `n"
    }
    $process = Start-Process $Command -ArgumentList $Arguments -NoNewWindow -PassThru -Wait

    if($process.ExitCode -ne 0){
        Throw "Error running: $Command $Arguments.  Returned Code: $($process.ExitCode)"
    }
}
