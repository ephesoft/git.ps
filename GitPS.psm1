using module .\Common.psm1

<#
.Synopsis
  Creates a new branch
.Description
  Creates a new branch in the current working directory's repository.
.Example
  New-GitBranch -Name 'MyExampleBranch'
  Creates a new branch named 'MyExampleBranch'.
.Parameter Name
  Name of the branch to create.
#>
function New-GitBranch {
  [CMDLetBinding(SupportsShouldProcess)]
  Param (
    [Parameter (Mandatory = $true)]
    [string]$Name
  )
  If ($PSCmdlet.ShouldProcess('Create branch')) {
    Invoke-NativeCommand -Command 'git' -Arguments ('branch', $Name)
  }
}

<#
.Synopsis
  Switch to a branch
.Description
  Switches to the branch with the specified name for the repository in the current working directory.
.Example
  Switch-GitBranch -Name 'MyExampleBranch'
  Checks out the MyExampleBranch branch.
.Parameter Name
  Name of the branch to checkout.
#>
function Switch-GitBranch {
  [CmdletBinding()]
  Param (
    [Parameter (Mandatory = $true)]
    [string]$Name
  )

  Invoke-NativeCommand -Command 'git' -Arguments ('checkout', $Name)
}

<#
.Synopsis
  Clones a repository
.Description
  Clones a repository into a folder named after the repo slug and places it in the current working directory.
.Example
  Get-GitRepository -URL 'https://me@bitbucket.org/MyProject/MyExampleBranch.git'
  Creates a new folder in the current working directory named MyExampleBranch and clones the repository into it.
.Parameter URL
  URL for the repository to clone.
#>
function Get-GitRepository {
  [CmdletBinding()]
  Param (
    [Parameter (Mandatory = $true)]
    [string]$URL
  )

  Invoke-NativeCommand -Command 'git' -Arguments ('clone', $URL)
}

<#
.Synopsis
  Adds changes to a commit
.Description
  Adds changes to a git commit.
.Example
  Add-GitChangesToCommit -message 'My Example Commit'
  Adds outstanding changes to a commit with the message 'My Example Commit'
.Parameter message
  The message added to the git commit.
#>
function Add-GitChangesToCommit {
  [CmdletBinding()]
  Param (
    [Parameter (Mandatory = $true)]
    [string]$message
  )
  Invoke-NativeCommand -Command 'git' -Arguments ('add', '.')
  Invoke-NativeCommand -Command 'git' -Arguments ('commit', '-m', "`"$message`"")
}

<#
.Synopsis
  Pushes local branch changes to the remote branch
.Description
  Pushes local branch changes to the remote branch for the repository in the current working directory.
.Example
  Push-GitBranch -Name 'MyExampleBranch'
  Pushes the branch MyExampleBranch from local to remote origin.
.Example
  Push-GitBranch -Name 'MyExampleBranch' -NoVerify
  Pushes the branch MyExampleBranch from local to remote origin and bypassess pre-commit and commit-msg hooks.
.Parameter Name
  Name of the branch to push.
.Parameter Destination
  Name of the remote to push to (defaults to origin).
.Parameter NoVerify
  Bypasses the pre-commit and commit-msg hooks.
#>

function Push-GitBranch {
  [CmdletBinding()]
  Param (
    [Parameter (Mandatory = $true)]
    [string]$Name,
    [string]$Destination = 'origin',
    [switch]$NoVerify

  )
  if ($NoVerify) {
    Invoke-NativeCommand -Command 'git' -Arguments ('push', '--set-upstream', $Destination, $Name, '--no-verify')
  }
  else {
    Invoke-NativeCommand -Command 'git' -Arguments ('push', '--set-upstream', $Destination, $Name)
  }
}

<#
.Synopsis
  Pulls changes from remote to local branch
.Description
  Pulls changes from remote to local branch for the repository in the current working directory.
.Example
  Update-GitBranch
  Pulls changes from the remote for the currently checked out local branch for the repsitory in the current working directory.
#>
function Update-GitBranch {
  [CMDLetBinding(SupportsShouldProcess)]
  Param ()
  If ($PSCmdlet.ShouldProcess('Update branch')) {
    Invoke-NativeCommand -Command 'git' -Arguments ('pull')
  }
}