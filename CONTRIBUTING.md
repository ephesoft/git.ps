# Making Changes
1. Make code changes in a new branch.
2. Test locally using `Invoke-Pester`.
3. Make a semantic version bump in GitPS.psd1.
4. Update CHANGELOG.md with changes.

# Deployment
1. Submit a pull request.
2. Make sure a build pipeline is green.
3. Merge changes when approved.
4. The pipeline will be automatically publish after the code is merged to master.

# Additional Resources
- Semantic Versioning - https://semver.org/