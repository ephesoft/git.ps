# GitPS #

Use PowerShell to manage Git repositories.

### What is this repository for? ###
Manage a git repository via a CI/CD pipeline or automated process.

### How do I get set up? ###
* Make sure you have PowerShell 7.0 or great installed
* Run `Install-Module GitPS`
* Run `Import-Module GitPS`
* Run `Get-Command -Module GitPS` to see available CMDlets
* See \build\test.ps1 for steps to run tests

### Making Changes ###
See CONTRIBUTING.md for more information.
