  Install-Module PSScriptAnalyzer -Force
  Install-Module Pester -Force
  Import-Module PSScriptAnalyzer
  Import-Module Pester
  $PesterConfig = [PesterConfiguration]::Default
  $PesterConfig.Output.Verbosity = 'Detailed'
  $PesterConfig.Run.Path = '.\*'
  $PesterConfig.Run.Exit = $true
  Invoke-Pester -Configuration $PesterConfig